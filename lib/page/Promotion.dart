import 'package:flutter/material.dart';
import 'package:prismoflutter/component/CustomAppBar.dart';
import '../Main.dart';
import '../component/BottomBar.dart';
import '../util/CounterInheritedWidget.dart';
import '../component/CounterText.dart';

class PromotionPage extends StatefulWidget {
  PromotionPage({Key key, this.title, this.color, this.bottomIndex})
      : super(key: key); // 設定傳入值
  final String title;
  final Color color;
  final bottomIndex;

  @override
  _PromotionPageState createState() => _PromotionPageState(); // 建立首頁的State
}

class _PromotionPageState extends State<PromotionPage> {
  int _counter = 0;
  bool _switchSelected = false; // 需建立在State下
  bool _checkboxSelected = true; // 否則調用setState也不會刷
  bool showToTopBtn = false; // 是否顯示置頂按鈕
  ScrollController _controller = new ScrollController(); // 滾動控制器

  @override
  void initState() {
    super.initState();
    print("我是initState");
    _controller.addListener(() {
      print(_controller.offset);
      if (_controller.offset < 1500 && showToTopBtn) {
        setState(() {
          showToTopBtn = false;
        });
      } else if (_controller.offset >= 1500 && !showToTopBtn) {
        setState(() {
          showToTopBtn = true;
        });
      }
    });
  }

  @override
  void didChangeDependencies() {
    print("我是didChangeDependencies");
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _switchChanged(value) {
    setState(() {
      _switchSelected = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: widget.title, context: context).create(),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            // Column為垂直佈局
            // 調適時可以查看物件框
            mainAxisAlignment: MainAxisAlignment.center, // col、row專用設定位置參數
            children: <Widget>[
              Container(
                color: Colors.purple,
                child: Align(
                  widthFactor: 2,
                  heightFactor: 2,
                  alignment: FractionalOffset(0.5, 0.5),
                  child: FlutterLogo(
                    size: 60,
                  ),
                ),
              ),
              Container(
                height: 20,
                alignment: Alignment.center,
              ),
              Center(
                child: Image.asset('images/haha.png'),
              ),
              Container(
                height: 20,
                alignment: Alignment.center,
              ),
              Container(
                color: Colors.green,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text.rich(
                      TextSpan(
                        style: TextStyle(
                            fontFamily: "MaterialIcons",
                            fontSize: 48.0,
                            color: Colors.orange),
                        children: [
                          TextSpan(text: "\uE6BB"),
                          TextSpan(
                            text: " = ",
                            style: TextStyle(
                              fontSize: 80.0,
                              letterSpacing: -40.0,
                              height: 0.5,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          TextSpan(text: "\uE90D"),
                        ],
                      ),
                    ),
                  ],
                ),
                height: 87,
                alignment: Alignment.center,
              ),
              Container(
                color: Colors.red,
                child: CounterInheritedWidget(
                  count: _counter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CounterText(),
                      Container(
                        width: 10,
                        alignment: Alignment.center,
                      ),
                      //每點擊一次，將count值遞增，然後重新build，CounterInheritedWidget的data將被更新
                      RaisedButton(
                        color: Colors.orange,
                        textColor: Colors.white,
                        splashColor: Colors.white30,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        onPressed: () => setState(() => _counter++),
                        child: Text("Increment"),
                      )
                    ],
                  ),
                ),
                height: 87,
                alignment: Alignment.center,
              ),
              Container(
                color: Colors.teal,
                child: GridView(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      childAspectRatio: 2.7,
                    ),
                    children: <Widget>[
                      Icon(Icons.ac_unit),
                      Icon(Icons.airport_shuttle),
                      Icon(Icons.all_inclusive),
                      Checkbox(
                        value: _checkboxSelected,
                        activeColor: Colors.red, // 選中時的顏色
                        checkColor: Colors.cyanAccent, // 打勾的颜色
                        onChanged: (value) {
                          setState(() {
                            _checkboxSelected = value;
                          });
                        },
                      ),
                      Icon(Icons.beach_access),
                      Icon(Icons.cake),
                      Icon(Icons.free_breakfast),
                      Padding(
                        padding: const EdgeInsets.only(right: 18.0, left: 18.0),
                        child: Switch(
                          value: _switchSelected,
                          onChanged: (value) => _switchChanged(value),
                        ),
                      ),
                    ]),
                height: 80,
                alignment: Alignment.center,
              ),
              Container(
                color: Colors.orangeAccent,
                child: PageStorage(
                  bucket: MyApp.bucket,
                  child: GridView.builder(
                    key: PageStorageKey('key_Promotion'),
                    controller: _controller,
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 300,
                      childAspectRatio: 2.5,
                    ),
                    itemCount: 100,
                    itemBuilder: (context, index) {
                      print(index);
                      return Icon(IconData(0xe55c + index,
                          fontFamily: 'MaterialIcons'));
                    },
                  ),
                ),
                height: 80,
                alignment: Alignment.center,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: !showToTopBtn
          ? null
          : FloatingActionButton(
              child: Icon(Icons.arrow_upward),
              onPressed: () {
                _controller.animateTo(
                  0, // 滾動位置
                  duration: Duration(milliseconds: 200), // 動畫時間
                  curve: Curves.ease, // 動畫的變化類型
                );
              }),
      bottomNavigationBar: BottomBar(index: widget.bottomIndex),
    );
  }
}
