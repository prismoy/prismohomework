import 'package:flutter/material.dart';
import 'package:prismoflutter/component/CustomAppBar.dart';
import 'package:prismoflutter/component/InputBox.dart';
import '../component/BottomBar.dart';

class MorePage extends StatefulWidget {
  MorePage({this.title, this.color, this.bottomIndex}) : super(); // 設定傳入值
  final String title;
  final Color color;
  final bottomIndex;

  @override
  _MorePageState createState() => _MorePageState(); // 建立首頁的State
}

class _MorePageState extends State<MorePage> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController adminController = new TextEditingController();
  FocusNode accountFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();
  FocusNode nameFocusNode = new FocusNode();
  FocusNode adminFocusNode = new FocusNode();
  bool nameIsError = false;
  bool adminIsError = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: widget.title, context: context).create(),
      body: Center(
        child: Theme(
          data: ThemeData(
            accentColor: Colors.lightGreenAccent,
          ),
          child: Form(
            child: Column(
              // Column為垂直佈局
              // 調適時可以查看物件框
              mainAxisAlignment: MainAxisAlignment.center, // col、row專用設定位置參數
              children: <Widget>[
                Center(
                  child: Image.network(
                    'https://www.crazyppt.com/wp-content/uploads/2018/10/1-160126131316.jpg',
                  ),
                ),
                Container(
                  color: Colors.green,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      TextField(
                        autofocus: false, // 自動焦點於此輸入框
                        cursorColor: Colors.grey[900],
                        focusNode: accountFocusNode,
                        decoration: InputBox().create(
                            accountFocusNode,
                            '請輸入帳號',
                            Icons.person,
                            Colors.grey[900],
                            Colors.grey[700],
                            false,
                            setState),
                      ),
                    ],
                  ),
                  height: 87,
                  alignment: Alignment.center,
                ),
                Container(
                  color: Colors.red,
                  child: TextField(
                    obscureText: true,
                    // 輸入的字將呈現星號
                    cursorColor: Colors.grey[100],
                    style: TextStyle(color: Colors.grey[100]),
                    focusNode: passwordFocusNode,
                    decoration: InputBox().create(
                        passwordFocusNode,
                        '請輸入密碼',
                        Icons.lock,
                        Colors.grey[100],
                        Colors.grey[300],
                        false,
                        setState),
                  ),
                  height: 87,
                  alignment: Alignment.center,
                ),
                Container(
                  color: Colors.purple,
                  child: TextFormField(
                      style: TextStyle(color: Colors.grey[100]),
                      focusNode: nameFocusNode,
                      controller: nameController,
                      decoration: InputBox().create(
                          nameFocusNode,
                          '請輸入用戶名',
                          Icons.face,
                          Colors.grey[100],
                          Colors.grey[300],
                          nameIsError,
                          setState),
                      validator: (text) {
                        // 驗證器
                        List<String> resultList = [];
                        String result = "  ";
                        if (text.trim().length < 1) {
                          resultList.add("用戶名不能為空");
                        }
                        for (int i = 0; i < resultList.length; i++) {
                          if (i > 0) {
                            result += "、";
                          }
                          result += resultList[i];
                        }
                        setState(() {
                          nameIsError = resultList.length != 0;
                        });
                        return nameIsError ? result : null;
                      }),
                  height: 87,
                  alignment: Alignment.center,
                ),
                Container(
                  color: Colors.lime,
                  child: TextFormField(
                      focusNode: adminFocusNode,
                      controller: adminController,
                      decoration: InputBox().create(
                          adminFocusNode,
                          '請輸入9527',
                          Icons.admin_panel_settings,
                          Colors.grey[900],
                          Colors.grey[700],
                          adminIsError,
                          setState),
                      validator: (text) {
                        // 驗證器
                        List<String> resultList = [];
                        String result = "  ";
                        if (text.trim().length != 4) {
                          resultList.add("字元長度不符");
                        }
                        if (text.trim() != "9527") {
                          resultList.add("終身代號錯誤");
                        }
                        for (int i = 0; i < resultList.length; i++) {
                          if (i > 0) {
                            result += "、";
                          }
                          result += resultList[i];
                        }
                        setState(() {
                          adminIsError = resultList.length != 0;
                        });
                        return adminIsError ? result : null;
                      }),
                  height: 87,
                  alignment: Alignment.center,
                ),
                Container(
                  child: Builder(
                    builder: (context) {
                      return RaisedButton(
                        color: Colors.orange,
                        textColor: Colors.white,
                        splashColor: Colors.white30,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        onPressed: () {
                          setState(() => {
                                if (Form.of(context).validate())
                                  {
                                    print('合格') // 驗證通過提交數據
                                  }
                                else
                                  {
                                    print('失格') // 驗證沒通過
                                  }
                              });
                        },
                        child: Text("確認"),
                      );
                    },
                  ),
                  height: 87,
                  alignment: Alignment.center,
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomBar(index: widget.bottomIndex),
    );
  }
}
