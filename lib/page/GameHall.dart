import 'package:flutter/material.dart';
import 'package:prismoflutter/component/CustomAppBar.dart';
import 'package:prismoflutter/component/HelloWorldText.dart';
import '../Main.dart';
import '../component/BottomBar.dart';

class GameHallPage extends StatefulWidget {
  GameHallPage({this.title, this.color, this.bottomIndex}) : super(); // 設定傳入值
  final String title;
  final Color color;
  final bottomIndex;

  @override
  _GameHallPageState createState() => _GameHallPageState(); // 建立首頁的State
}

class _GameHallPageState extends State<GameHallPage>
    with SingleTickerProviderStateMixin {
  int _counter = 0;
  AnimationController _animationController;
  List<Widget> widgetList = [];

  @override
  void initState() {
    super.initState();
    // 動畫執行時間1秒
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 10));
    _animationController.forward();
    _animationController.addListener(() => setState(() => {}));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose(); // 需放在最後才釋放
  }

  void _incrementCounter() {
    setState(() {
      // 需調用setState才會刷新畫面上的值
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    widgetList.addAll([
      Container(
        height: 30,
        margin:
            EdgeInsets.only(right: 10.0, left: 10.0, bottom: 10.0, top: 10.0),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          child: LinearProgressIndicator(
            backgroundColor: Colors.grey[200],
            valueColor: AlwaysStoppedAnimation(Colors.blue),
            value: _counter / 1000,
          ),
        ),
      ),
      Center(
        child: Image.network(
            'https://as.chdev.tw/web/article/0/6/4/0f7cd685-6273-4a28-a9cd-3c4f787227e61579508351.jpg'),
      ),
      Container(
        color: Colors.green,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              '按啦～哪次不按:',
            ),
          ],
        ),
        height: 87,
        alignment: Alignment.center,
      ),
      Container(
        color: Colors.red,
        child: Text(
          '$_counter',
          style: TextStyle(fontSize: 78),
        ),
        height: 87,
        alignment: Alignment.center,
      ),
      Container(
        height: 20,
        alignment: Alignment.center,
      ),
      ButtonTheme(
        minWidth: 56.0,
        height: 56.0,
        child: RaisedButton(
          onPressed: _incrementCounter,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(3600)),
          ),
          child: Icon(Icons.add),
          color: widget.color,
        ),
      ),
      Container(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey[200],
          valueColor: ColorTween(begin: Colors.grey, end: Colors.blue)
              .animate(_animationController),
          value: _animationController.value,
        ),
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
      )
    ]);

    List<String> nameList = [
      "Jack",
      "Mark",
      "John",
      "Howard",
      "Chris",
      "Kira",
      "Mike"
    ];

    for (int i = 0; i < nameList.length; i++) {
      widgetList.add(HelloWorldText(
          name: nameList[i], mainAxisAlignment: MainAxisAlignment.center));
      widgetList.add(HelloWorldText(
          name: nameList[i],
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min));
      widgetList.add(HelloWorldText(
          name: nameList[i],
          mainAxisAlignment: MainAxisAlignment.end,
          textDirection: TextDirection.rtl));
      widgetList.add(HelloWorldText(
          name: nameList[i],
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          style: TextStyle(fontSize: 30.0)));
    }

    return Scaffold(
      appBar: CustomAppBar(title: widget.title, context: context).create(),
      body: Center(
        child: Theme(
          data: ThemeData(
            accentColor: Colors.lightGreenAccent,
            fontFamily: 'Georgia', // 不會改
          ),
          child: PageStorage(
            bucket: MyApp.bucket,
            child: ListView.builder(
              key: PageStorageKey('key_GameHall'),
              itemCount: 35,
              addRepaintBoundaries: false,
              itemBuilder: (BuildContext context, int index) {
                print(index);
                return Column(
                  // Column為垂直佈局
                  // 調適時可以查看物件框
                  mainAxisAlignment: MainAxisAlignment.center,
                  // col、row專用設定位置參數
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    widgetList[index + widgetList.length - 35]
                  ],
                );
              },
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomBar(index: widget.bottomIndex),
    );
  }
}
