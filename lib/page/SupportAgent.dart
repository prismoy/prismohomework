import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prismoflutter/component/CustomAppBarShapeBorder.dart';
import 'package:prismoflutter/component/CustomBottomBarShapeBorder.dart';
import 'package:prismoflutter/component/CustomLayoutShapeBorder.dart';
import 'dart:ui' as ui;

class SupportAgentPage extends StatefulWidget {
  SupportAgentPage({this.title, this.color, this.bottomIndex})
      : super(); // 設定傳入值

  final String title;
  final Color color;
  final bottomIndex;

  @override
  _SupportAgentPageState createState() =>
      _SupportAgentPageState(); // 建立首頁的State
}

class _SupportAgentPageState extends State<SupportAgentPage> {
  final TextEditingController myController = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("===");
    print(MediaQuery.of(context));
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Container(
        child: Stack(
          children: [
            Scaffold(
              backgroundColor: Color.fromRGBO(53, 53, 99, 1),
              appBar: PreferredSize(
                preferredSize: Size.fromHeight(80.h), // here the desired height
                child: AppBar(
                  backgroundColor: Color.fromRGBO(38, 41, 77, 1),
                  shadowColor: Color.fromRGBO(132, 136, 225, 1),
                  shape: CustomAppBarShapeBorder(
                    firstWidth: 12.w,
                    firstWidth2: 50.w,
                    firstHeight: 30.h,
                    firstHeight2: 56.h,
                    cubicWidth: 24.w,
                    cubicWidth2: 40.w,
                    cubicHeight: 90.h,
                    secondWidth: 66.w,
                    secondWidth2: 48.w,
                    secondHeight: 48.h,
                    secondHeight2: 56.h,
                  ),
                  automaticallyImplyLeading: false,
                  bottom: PreferredSize(
                    preferredSize: Size.fromHeight(80.h),
                    // here the desired height
                    child: Container(
                      margin: EdgeInsets.only(bottom: 30.h),
                      child:Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Container(
                              child: Padding(
                                padding:
                                EdgeInsets.only(bottom: 16.h),
                                child: Icon(Icons.cloud, size: 30.h),
                              ),
                            ),
                            SizedBox(
                              width: 60.0.w,
                            ),
                            Container(
                              child: Padding(
                                padding:
                                EdgeInsets.only(bottom: 16.h),
                                child: Icon(Icons.menu, size: 30.h),
                              ),
                            ),
                          ],
                        ),
                        Positioned(
                          top: 12.h,
                          left: 0.0,
                          right: 0.0,
                          child: Container(
                            child: Icon(Icons.account_circle, size: 40.h),
                          ),
                        ),
                      ],
                    ),
                    ),
                  ),
                ),
              ),
              body: Container(
                decoration: new BoxDecoration(
                  gradient: new LinearGradient(
                    colors: [
                      Color.fromRGBO(53, 53, 99, 1),
                      Color(0xFF302f59),
                      Color(0xFF000000)
                    ],
                    begin: const FractionalOffset(0.5, 0),
                    end: const FractionalOffset(0, 0.5),
                  ),
                ),
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: 40.w, right: 40.w, bottom: 10.h),
                            child: Theme(
                              data: ThemeData(
                                  primaryColor: Colors.black38,
                                  inputDecorationTheme: InputDecorationTheme(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(63.h)),
                                    ),
                                  )),
                              child: Stack(
                                children: [
                                  DecoratedBox(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(63.0),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.white10,
                                              offset: Offset(-2.0, 2.0),
                                              blurRadius: 1.0)
                                        ]),
                                    child: Container(),
                                  ),
                                  DecoratedBox(
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        colors: [
                                          Color(0xFF191e3c),
                                          Color.fromRGBO(40, 46, 82, 1),
                                          Color.fromRGBO(40, 46, 82, 1),
                                          Color.fromRGBO(40, 46, 82, 1),
                                          Color.fromRGBO(40, 46, 82, 1),
                                        ],
                                        begin: FractionalOffset(1, 0),
                                        end: FractionalOffset(1, 1),
                                      ),
                                      borderRadius: BorderRadius.circular(63.0),
                                    ),
                                    child: Container(),
                                  ),
                                  TextFormField(
                                    cursorColor: Colors.white,
                                    onChanged: (value) {},
                                    textAlignVertical: TextAlignVertical(y: 0.36 * (MediaQuery.of(context).size.height / MediaQuery.of(context).size.width)
                                        - 0.5/(MediaQuery.of(context).size.height / MediaQuery.of(context).size.width)),
                                    decoration: InputDecoration(
                                      hintStyle: TextStyle(
                                          color: Color.fromRGBO(
                                              222, 222, 255, 0.8),
                                          fontSize: 15.sp),
                                      filled: true,
                                      fillColor: Color.fromRGBO(0, 0, 0, 0),
                                      hintText: "  Search...",
                                      suffixIcon: Icon(
                                        Icons.search,
                                        color:
                                            Color.fromRGBO(222, 222, 255, 0.8),
                                        size: 22.h,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          height: 54.h,
                          alignment: Alignment.center,
                        ),
                        Container(
                          child: ListView.builder(
                            padding: EdgeInsets.only(left: 22.w),
                            scrollDirection: Axis.horizontal,
                            itemCount: 2,
                            itemExtent: 290.w,
                            itemBuilder: (BuildContext context, int index) {
                              return Stack(
                                children: [
                                  Container(
                                    margin: EdgeInsets.fromLTRB(20.w,
                                        index < 1 ? 20.h : 60.h, 0.0, 0.0),
                                    padding: EdgeInsets.fromLTRB(
                                        24.w, 15.h, 20.w, 0.0),
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0x669396ca),
                                          offset: Offset(1.0, 1.0),
                                          blurRadius: 1.w,
                                          spreadRadius: 4.w,
                                        ),
                                        BoxShadow(
                                          color: Color(0x99302f59),
                                          offset: Offset(2.0, 1.0),
                                          blurRadius: 2.w,
                                          spreadRadius: 3.w,
                                        ),
                                      ],
                                      gradient: LinearGradient(
                                        colors: [
                                          Color(0xFF16162e),
                                          Color(0xFF574f95),
                                          Color(0xFFa4a6d4),
                                        ],
                                        begin: FractionalOffset(0, 1),
                                        end: FractionalOffset(1, 0),
                                      ),
                                      border: Border.all(
                                          color: Color.fromRGBO(0, 0, 0, 0),
                                          width: 3.w),
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(21.h),
                                        topRight: Radius.circular(63.h),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(21.w,
                                        index < 1 ? 23.h : 63.h, 3.w, 0.0),
                                    padding: EdgeInsets.fromLTRB(
                                        24.w, 15.h, 20.w, 0.0),
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        colors: [
                                          Color(0xFF16162e),
                                          Color(0xFF302f59),
                                          Color(0xFF383b75),
                                        ],
                                        begin: FractionalOffset(0, 1),
                                        end: FractionalOffset(1, 0),
                                      ),
                                      border: Border.all(
                                          color:
                                              Color.fromRGBO(161, 165, 210, 0),
                                          width: 3.w),
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(21.h),
                                        topRight: Radius.circular(63.h),
                                      ),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.all(9.h),
                                              decoration: BoxDecoration(
                                                color: Colors.black,
                                                border: Border.all(
                                                    color: Colors.black),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(16.h)),
                                              ),
                                              child: Icon(Icons.article,
                                                  size: 32.h),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  width: 130.w,
                                                  padding: EdgeInsets.only(
                                                      left: 24.w, top: 20.h),
                                                  child: Text(
                                                    'MyDocs',
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 24.sp,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 5.h,
                                                ),
                                                Container(
                                                  padding: EdgeInsets.only(
                                                      left: 24.w),
                                                  child: Text(
                                                    '3248 files, 26 folders',
                                                    style: TextStyle(
                                                      color: Colors.grey,
                                                      fontSize: 13.sp,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 40.h,
                                        ),
                                        Container(
                                          height: 7.h,
                                          width: 200.w,
                                          margin: EdgeInsets.only(
                                              left: 2.w, bottom: 1.h, top: 6.h),
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(21.h)),
                                            child: LinearProgressIndicator(
                                              backgroundColor:
                                                  Color(0xFF211f3e),
                                              valueColor:
                                                  AlwaysStoppedAnimation(
                                                      Color(0xFF5b78e0)),
                                              value: 0.6,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10.h,
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 130.w),
                                          alignment: Alignment.bottomRight,
                                          width: 70.w,
                                          child: Text(
                                            '60 GB free',
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 12.sp),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                          height: 360.h,
                        ),
                      ],
                    ),
                    Positioned(
                      bottom: 0.0,
                      top: 280.h,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        child: CustomPaint(
                          painter: CustomLayoutShapeBorder(
                              Color(0xFFe6e7f2), Color(0xFFcfd4eb)),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 0.0,
                      top: 280.h,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        child: CustomPaint(
                          painter: CustomLayoutShapeBorder2(),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 0.0,
                      top: 315.h,
                      right: 36.w,
                      left: 36.w,
                      child: Container(
                        child: GridView.builder(
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            childAspectRatio: 1.45 /
                                (MediaQuery.of(context).size.height /
                                    MediaQuery.of(context).size.width),
                          ),
                          itemCount: 9,
                          itemBuilder: (context, index) {
                            print(index);
                            return Column(
                              children: [
                                index == 7
                                    ? Container(
                                        decoration: BoxDecoration(
                                          color: Color(0x00ffffff),
                                        ),
                                      )
                                    : Container(
                                        padding: EdgeInsets.all(18.h),
                                        decoration: BoxDecoration(
                                          color: Color.fromRGBO(
                                              234, 235, 255, 0.8),
                                          border: Border.all(
                                              color: Color.fromRGBO(
                                                  255, 255, 255, 0.2)),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(36.h)),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0x22000000),
                                                offset: Offset(-4.0, 8.0),
                                                blurRadius: 10.0,
                                                spreadRadius: 0.5),
                                          ],
                                        ),
                                        child: Container(
                                          decoration: BoxDecoration(boxShadow: [
                                            BoxShadow(
                                                color: Color(0x22222277),
                                                offset: Offset(-5.0, 10.0),
                                                blurRadius: 20.0,
                                                spreadRadius: 1),
                                          ]),
                                          child: Icon(
                                              IconData(0xe55c + index,
                                                  fontFamily: 'MaterialIcons'),
                                              color: Colors.black,
                                              size: 56.h),
                                        ),
                                      ),
                                Container(
                                  height: 20.h,
                                ),
                                index == 7
                                    ? Text("")
                                    : Text(
                                        "Icon",
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black),
                                      ),
                                index == 7
                                    ? Text("")
                                    : Text(
                                        ".png",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: Colors.grey),
                                      ),
                              ],
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0.0,
              right: 0.0,
              left: 0.0,
              height: 70.h,
              child: Container(
                child: Scaffold(
                  backgroundColor: Colors.transparent,
                  bottomNavigationBar: BottomAppBar(
                    color: Color.fromRGBO(200, 210, 238, 0.8),
                    shape: CustomBottomBarShapeBorder(), // 底部導航欄打一個圓形的洞
                    child: Container(),
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 0.0,
              right: 0.0,
              left: 0.0,
              height: 80.h,
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.h),
                    width: 140.w,
                    height: 80.h,
                    child: RawMaterialButton(
                      shape: RoundedRectangleBorder(),
                      child: Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                              color: Color(0x33222277),
                              offset: Offset(1.0, 10.0),
                              blurRadius: 20.0,
                              spreadRadius: 0.1),
                        ]),
                        child: Icon(
                          Icons.home,
                          size: 35.h,
                          color: Color(0xFFd9ddee),
                        ),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  SizedBox(),
                  Container(
                    padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                    width: 140.w,
                    height: 80.h,
                    child: RawMaterialButton(
                      shape: RoundedRectangleBorder(),
                      child: Icon(
                        Icons.schedule,
                        size: 35.h,
                        color: Color(0xFFd9ddee),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween, //均分底部導航欄橫向空間
              ),
            ),
            Positioned(
              bottom: 30.h,
              right: 160.w,
              left: 160.w,
              height: 120.h,
              child: Container(
                child: Scaffold(
                  backgroundColor: Colors.transparent,
                  body: Container(
                    alignment: Alignment.center,
                    child: RawMaterialButton(
                      fillColor: Color.fromRGBO(207, 212, 236, 1),
                      shape: CircleBorder(),
                      elevation: 10,
                      child: Icon(
                        Icons.keyboard_backspace,
                        color: Color(0xFF434898),
                        size: 94.h,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 20.h,
              left: 68.w,
              child: Container(
                height: 5.h,
                width: 5.w,
                decoration: BoxDecoration(
                  color: Color(0xFFd9ddee),
                  shape: BoxShape.circle,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomLayoutShapeBorder2 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..shader = ui.Gradient.linear(
        Offset(90.w, -130.h),
        Offset(150.w, 150.h),
        [
          Color(0xFF999ab9),
          Color(0xFFcfd4eb),
        ],
      )
      ..style = PaintingStyle.fill;

    Path path = Path()..moveTo(0, -60.h); //0,60

    var firstStartPoint = new Offset(16.w, 0); //36,0
    var firstEndPoint = new Offset(90.w, 8.h); //100,0
    path.quadraticBezierTo(firstStartPoint.dx, firstStartPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy); // 左邊尖角

    path.lineTo(size.width - 150.w, 10.h); //80,0

    var secondStartPoint = new Offset(size.width - 30.w, 0); //-5,8
    var secondEndPoint = new Offset(size.width, 80.h); //0,80
    path.quadraticBezierTo(secondStartPoint.dx, secondStartPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy); // 右邊圓角

    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
