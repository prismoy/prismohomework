import 'package:flutter/material.dart';
import 'package:prismoflutter/component/CustomAppBar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart' show rootBundle;
import '../component/BottomBar.dart';

class HomePage extends StatefulWidget {
  HomePage({this.title, this.color, this.bottomIndex}) : super(); // 設定傳入值

  final String title;
  final Color color;
  final bottomIndex;

  @override
  _HomePageState createState() => _HomePageState(); // 建立首頁的State
}

class _HomePageState extends State<HomePage> {
  final TextEditingController myController = new TextEditingController();
  int _counter = 0;

  void loadAsset() async {
    String haha = await rootBundle.loadString('scripts/haha.json');
    print(haha);
  }

  @override
  void initState() {
    super.initState();
    _loadCounter();
    loadAsset();
    testError(); // 用來測試常的副程式
  }

  @override
  void deactivate() {
    super.deactivate();
    print("再見我被釋放了？");
  }

  @override
  void dispose() {
    super.dispose();
    print("再見我被釋放了");
  }

  void _loadCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _counter = (prefs.getInt('counter') ?? 0);
    });
  }

  void _incrementCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _counter = (prefs.getInt('counter') ?? 0) + 1;
    });
    await prefs.setInt('counter', _counter);
    print(myController.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: widget.title, context: context).create(),
      body: Center(
        // 創建置中子物件擺放的佈局
        child: SingleChildScrollView(
          child: Column(
            // Column為垂直佈局
            // 調適時可以查看物件框
            mainAxisAlignment: MainAxisAlignment.center, // col、row專用設定位置參數
            children: <Widget>[
              Center(
                child: Image.network(
                    'https://child.taiwan.net.tw/UserFiles/files/fi01.png'),
              ),
              Container(
                color: Colors.blue,
                child: Text(
                  '我的第一個APP',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                height: 87,
                alignment: Alignment.center,
              ),
              Container(
                color: Colors.green,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      '按啦～哪次不按:',
                      style: TextStyle(decoration: TextDecoration.lineThrough),
                    ),
                    Container(
                      width: 87,
                      child: TextField(
                        controller: myController,
                        decoration: InputDecoration(hintText: '請輸入...'),
                      ),
                    ),
                    FloatingActionButton(
                      onPressed: _incrementCounter,
                      tooltip: 'Increment',
                      child: Icon(Icons.add),
                      backgroundColor: widget.color,
                    ),
                  ],
                ),
                height: 87,
                alignment: Alignment.center,
              ),
              Container(
                color: Colors.red,
                child: Text(
                  '$_counter',
                  style: TextStyle(fontSize: 78),
                ),
                height: 87,
                alignment: Alignment.center,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomBar(index: widget.bottomIndex),
    );
  }

  testError() {
    try {
      Future.delayed(Duration(seconds: 1))
          .then((e) => Future.error("I'm async error 2"));
      Future.error("I'm async error 1");
      print('我還可以執行');
      throw "Haha My name is error";
      print('我執行不到了');
    } catch (e) {
      print(e + " 被 catch 到啦");
    }
  }
}
