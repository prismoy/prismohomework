import 'package:flutter/material.dart';

class MessagePage extends StatefulWidget {
  MessagePage({this.title, this.color}) : super(); // 設定傳入值

  final String title;
  final Color color;

  @override
  _MessagePageState createState() => _MessagePageState(); // 建立首頁的State
}

class _MessagePageState extends State<MessagePage> {
  @override
  void initState() {
    super.initState();
  }

  void _goBack() {
    Navigator.pop(context, "我是返回值");
  }

  void _showAlert() {
    ScaffoldMessengerState _state = ScaffoldMessenger.of(context);
    _state.showSnackBar(
      SnackBar(
        content: Text("我是SnackBar"),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        // 創建置中子物件擺放的佈局
        child: Column(
          // Column為垂直佈局
          // 調適時可以查看物件框
          mainAxisAlignment: MainAxisAlignment.center, // col、row專用設定位置參數
          children: <Widget>[
            Center(
              child: Image.network(
                  'https://pic3.zhimg.com/v2-7759486c8abe4bf9e7daad5a42687fa6_b.gif'),
            ),
            Container(
              color: widget.color,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    widget.title + '-按我返回:',
                    style: TextStyle(fontSize: 24),
                  ),
                  FlatButton(
                    onPressed: _goBack,
                    child: Icon(Icons.call_missed),
                  ),
                ],
              ),
              height: 87,
              alignment: Alignment.center,
            ),
            Container(
              height: 20,
              alignment: Alignment.center,
            ),
            Container(
              color: Colors.red,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    '按我顯示彈窗:',
                    style: TextStyle(fontSize: 24),
                  ),
                  FlatButton(
                    onPressed: _showAlert, // 調用彈窗方法
                    child: Icon(Icons.upgrade),
                  ),
                ],
              ),
              height: 87,
              alignment: Alignment.center,
            ),
            Flex(
              direction: Axis.horizontal, // 子Widget照水平排列
              children: <Widget>[
                Expanded(
                  flex: 1, // 佔該水平Flex寬的的1：2中三等份的一等份
                  child: Container(
                    height: 30.0, // 容器的高度
                    color: Colors.red, // 容器的背景顏色
                  ),
                ),
                Expanded(
                  flex: 2, // 佔該水平Flex寬三分之二寬
                  child: Container(
                    height: 30.0, // 容器的高度
                    color: Colors.green, // 容器的背景顏色
                  ),
                ),
              ],
            ),
            Padding( // 為SingleChildRenderObjectWidget
              padding: EdgeInsets.only(top: 20.0), //對子組件上方內距20
              child: Container(
                height: 160.0,
                //Flex的三個子widget，在垂直方向按2：1：1來佔用100像素的空間
                child: Flex(
                  direction: Axis.vertical, // 垂直Flex
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Container(
                        height: 300.0,
                        color: Colors.red,
                      ),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        height: 30.0,
                        color: Colors.green,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
