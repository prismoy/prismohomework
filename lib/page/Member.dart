import 'package:flutter/material.dart';
import 'package:prismoflutter/component/CustomAppBar.dart';
import 'package:provider/provider.dart';
import '../component/BottomBar.dart';
import '../util/CountChangeNotifier.dart';

class MemberPage extends StatefulWidget {
  MemberPage({this.title, this.color, this.bottomIndex}) : super(); // 設定傳入值
  final String title;
  final Color color;
  final bottomIndex;

  @override
  _MemberPageState createState() => _MemberPageState(); // 建立首頁的State
}

class _MemberPageState extends State<MemberPage> {
  final TextEditingController myController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: widget.title, context: context, showMessage: false).create(),
      body: DefaultTextStyle(
        style: TextStyle(
          color: Colors.black,
          fontSize: 20.0,
        ),
        child: Center(
          // 創建置中子物件擺放的佈局
          child: Consumer<CountChangeNotifier>(
            builder: (context, counter, _) {
              return Column(
                // Column為垂直佈局
                // 調適時可以查看物件框
                mainAxisAlignment: MainAxisAlignment.center,
                // col、row專用設定位置參數
                children: <Widget>[
                  Center(
                      child: Image.network(
                          'https://cdn-thumbnail.mamilove.com.tw/e_MlxcMkEGnyWAjr5jDs4ZdGqAY=/900x0/https://cdn-images.mamilove.com.tw/origin/blog/1162/1162-df5c71cf29-1547553767.jpg')),
                  Container(
                    color: Colors.blue,
                    child: Text(
                      'My first app',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    height: 87,
                    alignment: Alignment.center,
                  ),
                  Container(
                    color: Colors.green,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          '按啦～哪次不按:',
                          style:
                              TextStyle(decoration: TextDecoration.lineThrough),
                        ),
                        Container(
                          width: 87,
                          child: TextField(
                            controller: myController,
                            decoration: InputDecoration(hintText: '請輸入...'),
                          ),
                        ),
                        FloatingActionButton(
                          // onPressed: _incrementCounter,
                          onPressed: () {
                            Provider.of<CountChangeNotifier>(context,
                                    listen: false)
                                .increment();
                          },
                          tooltip: 'Increment',
                          child: Icon(Icons.add),
                          backgroundColor: widget.color,
                        ),
                      ],
                    ),
                    height: 87,
                    alignment: Alignment.center,
                  ),
                  Container(
                    color: Colors.red,
                    child: Text(
                      '${counter.count}',
                      style: TextStyle(fontSize: 78),
                    ),
                    height: 87,
                    alignment: Alignment.center,
                  ),
                ],
              );
            },
          ),
        ),
      ),
      bottomNavigationBar: BottomBar(index: widget.bottomIndex),
    );
  }
}
