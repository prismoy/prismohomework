import 'dart:async';
import 'package:flutter_screenutil/screenutil_init.dart';
import 'package:flutter/material.dart';
import 'package:prismoflutter/page/Promotion.dart';
import 'package:provider/provider.dart';
import 'util/CountChangeNotifier.dart';
import 'page/GameHall.dart';
import 'page/Home.dart';
import 'page/Member.dart';
import 'page/More.dart';

void main() {
  runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();
    runApp(MyApp());
  }, (Object obj, StackTrace stack) async {
    print("這個異常回傳 -> （" + obj + "）被 runZoned 處理了"); // 構建錯誤信息
  });
}

class MyApp extends StatelessWidget {
  static PageStorageBucket bucket = PageStorageBucket();

  // 應用程式入口
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(428, 926),
      allowFontScaling: true,
      builder: () => MultiProvider(
        providers: [ChangeNotifierProvider.value(value: CountChangeNotifier())],
        child: MaterialApp(
          theme: ThemeData(
            brightness: Brightness.dark,
            primarySwatch: Colors.amber, // 設定應用的主題
            appBarTheme: AppBarTheme(color: Colors.amber),
            textTheme: TextTheme(
              bodyText2:
              TextStyle(fontSize: 24.0, fontFamily: 'Hind'), // 會改到默認內文字
            ),
          ),
          routes: {
            // 註冊首頁路由
            "/": (context) =>
                HomePage(title: '首頁', color: Colors.purple, bottomIndex: 0),
            "GameHall": (context) => GameHallPage(
                title: '遊戲大廳', color: Colors.purpleAccent, bottomIndex: 1),
            "Promotion": (context) => PromotionPage(title: '優惠', bottomIndex: 2),
            "Member": (context) => MemberPage(title: '我的', bottomIndex: 3),
            "More": (context) => MorePage(title: '更多', bottomIndex: 4),
          },
          onGenerateRoute: (RouteSettings settings) {
            print('哭阿母母');
            return MaterialPageRoute(
                builder: (context) => pageContentBuilder(context));
          },
          navigatorObservers: [
            MyObserver(),
          ],
        ),
      ),
    );
  }

  pageContentBuilder(BuildContext context) {
    print('哭啊');
  }
}

class MyObserver extends NavigatorObserver {
  @override
  void didPush(Route route, Route previousRoute) {
    // 當調用Navigator.push時回調
    super.didPush(route, previousRoute);
    //可通過route.settings獲取路由相關內容
    // route.currentResult獲取返回內容
    // ....等等
    print('哭啊2');
  }

  @override
  void didPop(Route route, Route previousRoute) {
    // 當調用Navigator.pop時回調
    super.didPop(route, previousRoute);
    //可通過route.settings獲取路由相關內容
    // route.currentResult獲取返回內容
    // ....等等
    print('哭啊3');
  }
}
