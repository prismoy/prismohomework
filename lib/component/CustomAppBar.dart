import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prismoflutter/page/Message.dart';
import 'package:prismoflutter/page/SupportAgent.dart';

class CustomAppBar {
  CustomAppBar({this.title, this.context, this.showMessage = true})
      : super(); // 設定傳入值
  final title;
  final context;
  final showMessage;

  PreferredSizeWidget create() {
    return AppBar(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: 40,
            child: showMessage
                ? FlatButton(
                    onPressed: () async {
                      dynamic result = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MessagePage(
                                  title: '站內信', color: Colors.purple)));
                      print('我返回了 -> ' + '$result');
                    },
                    padding: EdgeInsets.all(0.0),
                    child: Icon(Icons.email),
                  )
                : null,
          ),
          Container(
            child: Text(title),
          ),
          Container(
            width: 40,
            child: FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SupportAgentPage(),
                    ),
                  );
                },
                padding: EdgeInsets.all(0.0),
                child: Icon(Icons.support_agent)),
          ),
        ],
      ),
    );
  }
}
