import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:ui' as ui;

class CustomLayoutShapeBorder extends CustomPainter {
  CustomLayoutShapeBorder(this.color1,this.color2): super();

  final color1,color2;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..shader = ui.Gradient.linear(
        Offset(90.w, -90.h),
        Offset(150.w, 150.h),
        [
          color1,
          color2,
        ],
      )
      ..style = PaintingStyle.fill;

    Path path = Path()..moveTo(0, -60.h); //0,60

    var firstStartPoint = new Offset(36.w, 0); //36,0
    var firstEndPoint = new Offset(100.w, 0); //100,0
    path.quadraticBezierTo(firstStartPoint.dx, firstStartPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy); // 左邊尖角

    path.lineTo(size.width - 80.w, 0); //80,0

    var secondStartPoint = new Offset(size.width - 5.w, 8.h); //-5,8
    var secondEndPoint = new Offset(size.width, 80.h); //0,80
    path.quadraticBezierTo(secondStartPoint.dx, secondStartPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy); // 右邊圓角

    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}