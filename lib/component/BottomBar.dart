import 'package:flutter/material.dart';

class BottomBar extends StatefulWidget {
  BottomBar({this.index}) : super(); // 設定傳入值
  final index;

  @override
  _BottomBarState createState() => _BottomBarState(); // 建立首頁的State
}

class _BottomBarState extends State<BottomBar> {
  int _currentIndex = 0; // 預設值
  final bottomBarPageName = ['/', 'GameHall', 'Promotion', 'Member', 'More'];
  final bottomBarText = ['首頁', '遊戲大廳', '優惠', '我的', '更多'];

  @override
  void initState() {
    super.initState();
    _currentIndex = widget.index;
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.home), label: bottomBarText[0]),
        BottomNavigationBarItem(
            icon: Icon(Icons.games), label: bottomBarText[1]),
        BottomNavigationBarItem(
            icon: Icon(Icons.local_play), label: bottomBarText[2]),
        BottomNavigationBarItem(
            icon: Icon(Icons.account_circle), label: bottomBarText[3]),
        BottomNavigationBarItem(
            icon: Icon(Icons.menu), label: bottomBarText[4]),
      ],
      currentIndex: _currentIndex,
      // 目前選擇頁索引值
      fixedColor: Colors.amber,
      // 選擇頁顏色
      unselectedItemColor: Colors.white,
      // 導航欄點擊事件
      onTap: _onItemClick,
      // 是否顯示未選中Label
      showUnselectedLabels: true,
      // Item的位置模式，fixed為平均，shifting會放大
      type: BottomNavigationBarType.fixed,
    );
  }

  void _onItemClick(int index) {
    setState(() {
      if (index != _currentIndex)
        Navigator.pushReplacementNamed(context, bottomBarPageName[index]);
    });
  }
}
