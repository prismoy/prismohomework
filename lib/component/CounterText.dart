import 'package:flutter/material.dart';
import '../util/CounterInheritedWidget.dart';

class CounterText extends StatefulWidget {
  @override
  _CounterTextState createState() {
    return _CounterTextState();
  }
}

class _CounterTextState extends State<CounterText> {
  @override
  Widget build(BuildContext context) {
    //使用InheritedWidget中的共享資料
    return Text(
        "Count: ${CounterInheritedWidget.of(context).count.toString()}");
  }

  //父或祖先widget中的InheritedWidget改變(updateShouldNotify返回true)時會被調用
  //如果在build中沒有使用CounterInheritedWidget，則此回調就不會被調用
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("這是CounterInheritedWidget下Widget的didChangeDependencies");
  }
}
