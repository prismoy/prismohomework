import 'package:flutter/material.dart';

class HelloWorldText extends StatefulWidget {
  HelloWorldText(
      {this.name,
      this.mainAxisAlignment = MainAxisAlignment.start,
      this.mainAxisSize = MainAxisSize.max,
      this.textDirection = TextDirection.ltr,
      this.crossAxisAlignment = CrossAxisAlignment.center,
      this.style})
      : super(); // 設定傳入值
  final name;
  final mainAxisAlignment;
  final mainAxisSize;
  final textDirection;
  final crossAxisAlignment;
  final style;

  @override
  _HelloWorldTextState createState() {
    return _HelloWorldTextState();
  }
}

class _HelloWorldTextState extends State<HelloWorldText> {
  @override
  Widget build(BuildContext context) {
    return Row(
      textDirection: widget.textDirection,
      mainAxisSize: widget.mainAxisSize,
      mainAxisAlignment: widget.mainAxisAlignment,
      crossAxisAlignment: widget.crossAxisAlignment,
      children: <Widget>[
        Text(
          " hello world ",
          style: widget.style,
        ),
        Text(" I am ${widget.name} "),
      ],
    );
  }
}
