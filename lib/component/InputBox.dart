import 'package:flutter/material.dart';

class InputBox {
  create(
      focusNode, hinText, icon, focusColor, notFocusColor, isError, setState) {
    focusNode.addListener(() {
      setState(() {
        // 不要問我為什麼這樣就可以動態改變
      });
    });
    return InputDecoration(
      hintText: hinText,
      hintStyle: TextStyle(
        color: isError
            ? Colors.red
            : focusNode.hasFocus
                ? focusColor
                : notFocusColor,
      ),
      prefixIcon: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            icon,
            color: isError
                ? Colors.red
                : focusNode.hasFocus
                    ? focusColor
                    : notFocusColor,
          ),
          Text(
            " 標題 ",
            style: TextStyle(
              fontSize: 20,
              color: isError
                  ? Colors.red
                  : focusNode.hasFocus
                      ? focusColor
                      : notFocusColor,
            ),
          ),
        ],
      ),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: notFocusColor),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: focusColor, width: 2.0),
      ),
    );
  }
}
