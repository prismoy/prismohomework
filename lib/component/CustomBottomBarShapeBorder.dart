import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomBottomBarShapeBorder extends NotchedShape {
  @override
  Path getOuterPath(Rect rect, Rect guest) {
    Path path = Path();
    path.lineTo(0, 16.h); //0,16

    var firstStartPoint = new Offset(12.w, 0); //12,0
    var firstEndPoint = new Offset(50.w, 0); //50,0
    path.quadraticBezierTo(firstStartPoint.dx, firstStartPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy); // 左邊尖角

    path.lineTo(rect.width / 2 - 112.w, 0); //112,0

    path.quadraticBezierTo(rect.width / 2 - 76.w, 0, rect.width / 2 - 62.w,
        18.h); // 中間左邊圓角 76,0 62,18

    path.cubicTo(
        rect.width / 2 - 38.w,
        60.h,
        rect.width / 2 + 38.w,
        60.h,
        // 38,60 38,60 62,18
        rect.width / 2 + 62.w,
        18.h);

    path.quadraticBezierTo(rect.width / 2 + 76.w, 0, rect.width / 2 + 112.w,
        0); // 中間又邊圓角 76,0 112,0

    path.lineTo(rect.width - 40.w, 0); //40,0

    var secondStartPoint = new Offset(rect.width, 0); //0,0
    var secondEndPoint = new Offset(rect.width, -16.h); //0,16
    path.quadraticBezierTo(secondStartPoint.dx, secondStartPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy); // 右邊圓角

    path.lineTo(rect.width, rect.height);
    path.lineTo(0, rect.height);
    path.close();
    return path;
  }
}
