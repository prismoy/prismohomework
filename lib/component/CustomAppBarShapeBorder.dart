import 'package:flutter/material.dart';

class CustomAppBarShapeBorder extends ContinuousRectangleBorder {
  CustomAppBarShapeBorder(
      {this.firstWidth,
        this.firstWidth2,
        this.firstHeight,
        this.firstHeight2,
        this.cubicWidth,
        this.cubicWidth2,
        this.cubicHeight,
        this.secondWidth,
        this.secondWidth2,
        this.secondHeight,
        this.secondHeight2})
      : super();

  final firstWidth,
      firstWidth2,
      firstHeight,
      firstHeight2,
      cubicWidth,
      cubicWidth2,
      cubicHeight,
      secondWidth,
      secondWidth2,
      secondHeight,
      secondHeight2;

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    Path path = Path();
    path.lineTo(0, rect.height);

    var firstStartPoint = new Offset(firstWidth, rect.height - firstHeight);
    var firstEndPoint = new Offset(firstWidth2, rect.height - firstHeight);
    path.quadraticBezierTo(firstStartPoint.dx, firstStartPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy); // 左邊尖角

    path.lineTo(rect.width / 2 - secondWidth, rect.height - firstHeight);

    path.quadraticBezierTo(
        rect.width / 2 - secondWidth2,
        rect.height - firstHeight,
        rect.width / 2 - cubicWidth2,
        rect.height - secondHeight); // 中間左邊圓角

    path.cubicTo(
        rect.width / 2 - cubicWidth,
        rect.height - cubicHeight,
        rect.width / 2 + cubicWidth,
        rect.height - cubicHeight,
        rect.width / 2 + cubicWidth2,
        rect.height - secondHeight);

    path.quadraticBezierTo(
        rect.width / 2 + secondWidth2,
        rect.height - firstHeight,
        rect.width / 2 + secondWidth,
        rect.height - firstHeight); // 中間又邊圓角

    path.lineTo(rect.width - cubicWidth2, rect.height - firstHeight);

    var secondStartPoint = new Offset(rect.width, rect.height - firstHeight);
    var secondEndPoint = new Offset(rect.width, rect.height - secondHeight2);
    path.quadraticBezierTo(secondStartPoint.dx, secondStartPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy); // 右邊圓角

    path.lineTo(rect.width, 0);

    path.close();
    return path;
  }
}
