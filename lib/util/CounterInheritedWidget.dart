import 'package:flutter/material.dart';

class CounterInheritedWidget extends InheritedWidget {
  CounterInheritedWidget({@required this.count, Widget child})
      : super(child: child);

  // 共享的資料，計數的值
  final count;

  // 獲取CounterInheritedWidget實例的方法,方便widget樹中的子widget獲取共享的資料
  static CounterInheritedWidget of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<CounterInheritedWidget>();
  }

  // 監聽關聯的值的變化，決定是否通知樹中有依賴共享數據的子widget (true 就通知)
  @override
  bool updateShouldNotify(CounterInheritedWidget oldWidget) {
    return oldWidget.count != count;
  }
}
